# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

inherit versionator

DATESTAMP="-$(get_version_component_range 4)"
MY_PV=$(get_version_component_range 1-2)-$(get_version_component_range 3)
MY_P="LBarcd-${MY_PV}-Linux_x86_64${DATESTAMP}.tar.gz"

DESCRIPTION="lanbilling - billing system for internet/telephony service providers"
HOMEPAGE="http://www.lanbilling.ru/"
SRC_URI="${MY_P}"
RESTRICT="fetch"

RESTRICT="fetch binchecks"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

S=${WORKDIR}

pkg_nofetch() {
	elog "Please download ${A} from ${HOMEPAGE}"
	elog "and move it to ${DISTDIR}"
}

src_install() {
	dodir etc
	mv etc/billing.conf.LBarcd.sample "${ED}"/etc/billing.conf.LBarcd || die

	dodir usr
	mv ./usr/* "${ED}"/usr || die

	newinitd "${FILESDIR}/LBarcd.initd" LBarcd || die
	newconfd "${FILESDIR}/LBarcd.confd" LBarcd || die

}
