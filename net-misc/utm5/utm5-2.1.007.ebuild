# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="NetUP UTM - universal billing system for Internet Service Providers."
HOMEPAGE="www.netup.ru"
SRC_URI="${P}.tar.bz2"

LICENSE="NETUP"
SLOT="0"
KEYWORDS="x86 amd64"
IUSE=""

RESTRICT="fetch binchecks"

RDEPEND="
	x86? ( dev-libs/openssl
	sys-libs/zlib
	dev-libs/libxslt )
	amd64? ( app-emulation/emul-linux-x86-baselibs )
	app-admin/sudo
	dev-perl/DBD-mysql
	|| ( dev-db/mysql
	dev-db/postgresql )"

S=${WORKDIR}

pkg_nofetch() {
	elog "Please download ${A} from:"
	elog "http://www.netup.ru/"
	elog "and move it to ${DISTDIR}"
}

pkg_setup() {
	local stop_process=""
	for process in utm5_radius utm5_rfw utm5_core; do
		if $(pgrep ${process} >/dev/null 2>&1); then
			stop_process+="${process} "
		fi
	done

	if [[ -n ${stop_process} ]]; then
		ewarn "You did not stop ${process}."
		ewarn "Please stop all process with ${process} in"
		ewarn "their names and then try again."
		die "Processes are not stoped."
	fi
}

src_install() {
	for conf in {utm5,radius5,rfw5,web5}.cfg; do
		insinto /etc/utm5/
		doins netup/utm5/${conf} || die
		dosym /etc/utm5/${conf} /netup/utm5/${conf}
		rm netup/utm5/${conf}
	done

	dodir /netup
	cp -a netup "${D}" || die

	doinitd "${FILESDIR}/utm5_core" "${FILESDIR}/utm5_radius" "${FILESDIR}/utm5_rfw" || die
	doconfd "${FILESDIR}/utm5_rfw.conf" || die

	dodir /var/www/netup
	cp -a usr/local/apache/* "${D}"/var/www/netup || die

	keepdir /netup/utm5/backup
	keepdir /netup/utm5/db
	keepdir /netup/utm5/log
}

pkg_preinst() {
	if [[ ! -x /netup/utm5/bin/utm5_core ]] ; then
		elog "If this is your first instalation of utm5 please run:"
		elog "mysqladmin create UTM5"
		elog "mysql UTM5 < /netup/utm5/UTM5_MYSQL.sql"
		elog "mysql UTM5 < your_reg_file.sql"
		elog "to initialise mysql database. Or"
		elog "createdb -U postgres UTM5"
		elog "psql UTM5 < /netup/utm5/UTM5_MYSQL.sql"
		elog "psql UTM5 < your_reg_file.sql"
		elog "to initialise postgresql database."
	else
		elog "Now, please, update your database with command"
		elog "mysql -f UTM5 < /netup/utm5/UTM5_MYSQL_update.sql"
		elog "if you are using mysql database or"
		elog "psql -f /netup/utm5/UTM5_PG_update.sql UTM5"
		elog "if you are using postgresql."
		elog ""
		elog "Please note. You need to use updated UTM5_Admin.jar also."
	fi
}

pkg_postinst() {
	echo
	elog "To start utm5_core automaticaly during booting you need to run:"
	elog "rc-update add utm5_core default"
	echo
	ewarn "Note: Configuration files are in /etc/utm5."
	echo
	elog "Thank you for choosing utm5."
}
