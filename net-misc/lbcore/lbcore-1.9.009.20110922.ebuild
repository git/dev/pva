# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

WEBAPP_MANUAL_SLOT="yes"
inherit versionator webapp depend.php rpm

DATESTAMP="-$(get_version_component_range 4)"
MY_PV=$(get_version_component_range 1-2)-$(get_version_component_range 3)
MY_P="LBcore-${MY_PV}-Linux_x86_64${DATESTAMP}.rpm"

DESCRIPTION="lanbilling - billing system for internet/telephony service providers"
HOMEPAGE="http://www.lanbilling.ru/"
SRC_URI="${MY_P}"

RESTRICT="fetch binchecks"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

S=${WORKDIR}

pkg_nofetch() {
	elog "Please download ${A} from ${HOMEPAGE}"
	elog "and move it to ${DISTDIR}"
}

src_install() {
	dodir etc
	mv etc/billing.conf.LBcore.sample "${ED}"/etc/billing.conf || die
	rm -rf etc

	pushd usr/local/billing/phpclient >/dev/null
	webapp_src_preinst
	mkdir admin/files
	SERVEROWNED_LIST="$(find admin/files admin/templates -type d) admin/users_reports"
	mv * "${ED}"${MY_HTDOCSDIR} || die
	webapp_serverowned $(printf "${MY_HTDOCSDIR}/%s " ${SERVEROWNED_LIST})
#	webapp_configfile ${MY_HTDOCSDIR}/soap/api3.wsdl
	webapp_src_install
	popd >/dev/null

	mv ./usr/* "${ED}"/usr || die

	newinitd "${FILESDIR}/LBcore.initd" LBcore
	newconfd "${FILESDIR}/LBcore.confd" LBcore

	chmod -R ugo=rX "${ED}/usr/share/webapps/"
}
