# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/www/viewcvs.gentoo.org/raw_cvs/gentoo-x86/net-analyzer/netdude/Attic/netdude-0.4.8a.ebuild,v 1.3 2010/04/06 17:33:38 ssuominen dead $

EAPI="2"

inherit eutils autotools

DESCRIPTION="Netdude is a front-end to the libnetdude packet manipulation library"
HOMEPAGE="http://netdude.sourceforge.net/index.html"
SRC_URI="mirror://sourceforge/netdude/${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc"

DEPEND=">=net-libs/libnetdude-0.10a
	net-libs/libpcapnav
	dev-libs/glib:1
	x11-libs/gtk+:1
	net-libs/libpcap
	virtual/fam
	doc? ( dev-util/gtk-doc )"

RDEPEND="${DEPEND}
	net-analyzer/tcpdump
	>=x11-libs/libX11-1.0.0
	>=x11-libs/libXext-1.0.0"

src_prepare() {
	# patches were sent to mailing list
	# http://sourceforge.net/mailarchive/forum.php?forum_name=netdude-devel
	epatch "${FILESDIR}/${P}-system-libltdl.patch"
	epatch "${FILESDIR}/${P}-pkddatadir.patch"
	eautoreconf
}

src_configure() {
	econf \
		--without-included-ltdl \
		$(use_enable doc gtk-doc)
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"
	dodoc AUTHORS ChangeLog README ROADMAP TODO || die
	use doc || rm -rf "${D}"/usr/share/gtk-doc
}
