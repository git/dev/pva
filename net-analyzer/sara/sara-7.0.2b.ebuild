# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="Security Auditor's Research Assistant is a derived work of SATAN"
SRC_URI="http://www-arc.com/sara/downloads/${P}.tgz"
HOMEPAGE="http://www-arc.com/sara/"

SLOT="0"
LICENSE="satan"
KEYWORDS="~amd64 ~ppc ~x86"
IUSE="X"

# Utility that is still required ypwhich strings ftp
DEPEND="app-shells/tcsh
		dev-libs/openssl
		net-misc/netkit-rsh
		net-misc/netkit-telnetd
		net-misc/netkit-fingerd
		net-misc/netkit-rusers
		net-misc/netkit-fingerd
		net-ftp/netkit-tftp
		net-ftp/ftp
		net-dns/bind-tools
		net-fs/nfs-utils
		net-fs/samba
		app-arch/sharutils
		net-analyzer/nmap
		net-ftp/ftp
		dev-perl/Net-SSLeay
		X? ( || ( ( x11-libs/libX11
					x11-proto/xproto
					x11-apps/xhost )
				  virtual/x11 ) )"

src_compile(){
	econf --prefix=${SARA_HOME} || die "sara ./configure(ation) failed"
	emake -j1 || die "make failed"
}

src_install(){
	# Very ugly hack around bad filename ("\n.html") in html/nvd.
	mkdir html/nvd.tmp && \
	mv html/nvd/ARC-* html/nvd/CVE-* html/nvd/NOCVE.html html/nvd.tmp && \
	rm -rf html/nvd/* && \
	mv html/nvd.tmp/* html/nvd/ && \
	rmdir html/nvd.tmp || die "Did not manage to remove \"\n.html\" :) "

	mkdir "${D}"/opt
	make PREFIX="${D}"/opt install

	dosbin "${FILESDIR}"/{sara,sara_add_user,sara_reconfig}

	# Moving docs to the better location.
	rm -rf "${D}"/opt/sara/docs
	dodoc docs/{README,README.EXTENSIONS,README.SMB,README.SSS,CHANGES}
	dodoc docs/CHANGES.OLD/*
	doman docs/sara.8
}

pkg_postinst() {
	elog "To access the documentation start sara in daemon mode 'sara -D'"
	elog "and point your browser to http://localhost:666. Note: Don't forget"
	elog "to add some user with sara_add_user."
	echo
	elog "If you have the tcpd wrappers or some other mechanism that does a reverse"
	elog "finger, turn off that feature before running SARA!"
}

pkg_postrm() {
	elog "Sara writes it's configuration and other runtime files in /opt/sara/"
	elog "So to remove sara completely you need to run \"rm -rf /opt/sara/\""
}
