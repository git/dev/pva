# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/net-analyzer/nessus-libraries/nessus-libraries-2.2.9.ebuild,v 1.2 2007/04/11 13:41:07 welp Exp $

inherit toolchain-funcs

DESCRIPTION="A remote security scanner for Linux (nessus-libraries)"
HOMEPAGE="http://www.nessus.org/"
SRC_URI="mirror://gentoo/${P}.tar.gz"
SLOT="0"
LICENSE="GPL-2"
KEYWORDS="~alpha ~amd64 ~ppc ~ppc64 ~sparc ~x86 ~x86-fbsd"
IUSE="bindist"

# Hard dep on SSL since libnasl won't compile when this package is emerged -ssl.
RDEPEND=">=dev-libs/openssl-0.9.6d"
DEPEND="sys-devel/flex
		sys-devel/bison
		sys-libs/zlib
		dev-libs/gmp"
S=${WORKDIR}/${PN}

src_compile() {
	export CC="$(tc-getCC)"
	econf $(use_enable bindist cipher) \
		--with-ssl \
		--disable-zlib \
		--disable-gmp \
		|| die "econf failed"
	emake || die "emake failed"
}

src_install() {
	emake DESTDIR="${D}" install || die "failed to install"
	dodoc README*
	# Package manager should cope with uninstall process:
	rm "${D}"/usr/sbin/uninstall-nessus
}
