# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/net-analyzer/nessus-core/nessus-core-2.2.9.ebuild,v 1.3 2007/04/28 18:57:39 tove Exp $

inherit eutils toolchain-funcs

DESCRIPTION="A remote security scanner for Linux (nessus-core)"
HOMEPAGE="http://www.nessus.org/"
SRC_URI="mirror://gentoo/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~alpha ~amd64 ~ppc ~ppc64 ~sparc ~x86 ~x86-fbsd"
IUSE="X tcpd debug"
DEPEND="~net-analyzer/nessus-libraries-${PV}
	~net-analyzer/libnasl-${PV}
	tcpd? ( sys-apps/tcp-wrappers )
	X? ( =x11-libs/gtk+-2* )
	!net-analyzer/nessus-client"

S=${WORKDIR}/${PN}

src_unpack() {
	unpack ${A}

	cd "${S}"
	epatch "${FILESDIR}"/${P}-doc-path.patch
}

src_compile() {
	export CC="$(tc-getCC)"
	econf $(use_enable tcpd tcpwrappers) \
		$(use_enable debug) \
		$(use_enable X gtk) \
		|| die "configure failed"
	emake -j1 || die "emake failed"
}

src_install() {
	make DESTDIR="${D}" install || die "Install failed nessus-core"
	dodoc README* UPGRADE_README CHANGES
	dodoc doc/*.txt doc/ntp/*
	newinitd "${FILESDIR}"/nessusd-r7 nessusd
	keepdir /var/lib/nessus/logs
	keepdir /var/lib/nessus/users
	# newer version of this header is provided by nessus-libraries
	rm "${D}"/usr/include/nessus/includes.h
}
