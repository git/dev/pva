# Copyright 1999-2007 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sys-apps/dchroot/dchroot-0.12.1.ebuild,v 1.1 2007/07/08 21:37:19 phreak Exp $

inherit toolchain-funcs

DESCRIPTION="Run-time shared library for locking devices"
HOMEPAGE="http://packages.debian.org/unstable/libs/liblockdev1"
SRC_URI="mirror://debian/pool/main/l/${PN}/${PN}_${PV}.orig.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

src_compile() {
	emake CC="$(tc-getCC)" CFLAGS="${CFLAGS}"
}

src_install() {
	make basedir="${D}"/usr/ install || die
	doman docs/lockdev.3
	dodoc AUTHORS ChangeLog ChangeLog.old README.debug docs/LSB.991201
}
