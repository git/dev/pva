# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils toolchain-funcs

DESCRIPTION="converter that transforms UNIX-style manpages from the DocBook SGML format into n/troff man macros"
HOMEPAGE="http://packages.debian.org/source/sid/docbook-to-man http://www.oasis-open.org/docbook/tools/dtm/"
# Use debian location instead of http://www.oasis-open.org/docbook/tools/dtm/ as
# it provides version component.
SRC_URI="http://ftp.de.debian.org/debian/pool/main/d/${PN}/${PN}_${PV%_p*}.orig.tar.gz
		http://ftp.de.debian.org/debian/pool/main/d/${PN}/${PN}_${PV%_p*}-${PV#*_p}.diff.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~x86"
IUSE=""
DEPEND="app-text/opensp
		=app-text/docbook-sgml-dtd-4.1*
		=app-text/docbook-sgml-dtd-3.1*"
RDEPEND="${DEPEND}"

S=${WORKDIR}/${P%_p*}.orig

src_unpack() {
	unpack ${A}

	cd "${S}"
	EPATCH_OPTS="-p1" epatch "${WORKDIR}"/${PN}_${PV%_p*}-${PV#*_p}.diff
	for dpatch in debian/patches/??-*.dpatch; do
		epatch "${S}"/${dpatch}
	done
	find -name Makefile -exec \
		sed -i -e "s:\(CC[[:space:]]*=\).*:\1 $(tc-getCC):" \
			-e "s:\(CFLAGS[[:space:]]*=.*\):\1 ${CFLAGS}:" \
			-e "s:\(LDFLAGS[[:space:]]*=.*\):\1 ${LDFLAGS}:" \{\} \;
}

src_install() {
	dodir /usr/bin /usr/share/sgml
	# TODO: FIXME:
	# Hacks to set paths inside docbook-to-man or it doesn't work...
	sed -i -e 's:dtd/4.1:sgml-dtd-4.1:' \
		-e 's:\(^CATALOG=\).*:\1/etc/sgml/sgml-docbook-4.1.cat:' \
		-e 's:-croff.cmap:-c/usr/share/sgml/transpec/roff.cmap:' \
		-e 's:-sroff.sdata:-s/usr/share/sgml/transpec/roff.sdata:' \
		-e 's:-tdocbook-to-man.ts:-t/usr/share/sgml/transpec/docbook-to-man.ts:'	\
		    cmd/docbook-to-man
	emake install ROOT="${D}"/usr

	doman Doc/{docbook-to-man.1,instant.1,transpec.1} Instant/tptregexp/regexp.3
	dodoc README{,.ANS}
}
