# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="1"
inherit eutils subversion

DESCRIPTION="LightLang - electronic dictionary system"
HOMEPAGE="http://lightlang.org.ru"
SRC_URI="ftp://ftp.etc.edu.ru/pub/soft/for_linux/lightlang/apps/stable/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE="X"

DEPEND=">=dev-python/PyQt4-4.2.2
		>=dev-python/sip-4.6
		media-sound/sox
		dev-python/python-xlib
		X? ( x11-terms/xterm )"
RDEPEND="${DEPEND}"

src_unpack() {
	unpack ${A}
	cd "${S}"
	find -name "Makefile.in" -print0 | xargs -0 \
		sed -i -e 's:\(INS_[[:alnum:]_]*=\)\(@prefix@[[:alnum:]\/]\):\1$(DESTDIR)/\2:'

	sed -i 's:\($(CC) $(CFLAGS)\)\( -o $(SL_PROG) $(SL_SRC)\):\1 $(LDFLAGS)\2:'	\
			apps/sl/Makefile.in || die

	epatch "${FILESDIR}"/${P}-man-pages-inst.patch
}

src_compile() {
	local myconf
	if use X; then
		myconf="--with-gui-flag=yes --with-gui-terminal=xterm"
	else
		myconf="--with-gui-flag=no"
	fi
	econf --with-audio-player=play \
		${myconf} || die

	emake || die
}

src_install() {
	emake DESTDIR="${D}" install || die "Install failed"
}

pkg_postinst() {
	elog "Now run 'llrepo' to install dictionaries and enjoy."
}
