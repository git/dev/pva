# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"
inherit eutils versionator

DESCRIPTION="LightLang - electronic dictionary system"
HOMEPAGE="http://lightlang.org.ru"
if [[ "${PV}" =~ (_p)([0-9]+) ]] ; then
	inherit subversion
	SRC_URI=""
	MTSLPT_REV=${BASH_REMATCH[2]}
	ESVN_REPO_URI="http://lightlang.googlecode.com/svn/trunk/${PN}/@${MTSLPT_REV}"
else
	MY_PV=$(get_version_component_range 1-3)-rev$(get_version_component_range 4)-rc${PV##*_rc}
	SRC_URI="http://lightlang.googlecode.com/files/lightlang-${MY_PV}.tar.bz2"
	S=${WORKDIR}/${PN}-${MY_PV}
fi

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="gtk qt4"

DEPEND="media-sound/sox
	dev-lang/python
	qt4? ( >=dev-python/PyQt4-4.2.2
	>=dev-python/sip-4.6
	dev-python/python-xlib )"
RDEPEND="${DEPEND}"

PDEPEND="gtk? ( app-text/slog )"

src_configure() {
	local myconf
	use qt4 && myconf="--with-gui-flag=yes" || myconf="--with-gui-flag=no"
	econf --with-audio-player=play ${myconf}
}

src_install() {
	emake DESTDIR="${D}" install || die "Install failed"
}

pkg_postinst() {
	elog "Now run 'llrepo' to install dictionaries and enjoy."
}
