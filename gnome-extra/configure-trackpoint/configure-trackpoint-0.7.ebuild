# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"

DESCRIPTION="Gnome TrackPoint configuration tool"
HOMEPAGE="http://tpctl.sourceforge.net/configure-trackpoint.html"
SRC_URI="mirror://sourceforge/tpctl/files/${PN}/${PV}/${P}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="gnome-base/libgnomeui"
RDEPEND="sys-fs/sysfsutils"

src_install() {
	emake DESTDIR="${D}" install || die
	dodoc AUTHORS ChangeLog || die
}
