# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/www/viewcvs.gentoo.org/raw_cvs/gentoo-x86/net-libs/libnetdude/Attic/libnetdude-0.10a.ebuild,v 1.2 2010/04/06 17:32:50 ssuominen dead $

EAPI="2"
inherit autotools

DESCRIPTION="Library for manipulating libpcap/tcpdump trace files"
HOMEPAGE="http://netdude.sourceforge.net/index.html"
SRC_URI="mirror://sourceforge/netdude/${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND=">=net-libs/libpcapnav-0.8
	net-analyzer/tcpdump	
	dev-libs/glib:1
	doc? ( dev-util/gtk-doc )"
RDEPEND="${DEPEND}"

IUSE="doc"

src_prepare() {
	epatch "${FILESDIR}/${P}-system-ltdl.patch"
	epatch "${FILESDIR}/${P}-pkg-libdir.patch"
	eautoreconf
}

src_configure() {
	econf \
		--without-included-ltdl \
		--with-tcpdump=/usr/sbin/tcpdump \
		$(use_enable doc gtk-doc)
}

src_compile() {
	emake || die
	use doc && { cd docs && emake docs || die; }
}

src_test() {
	# make tests exists but basically builds examples... Skip make tests
	:
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"
	dodoc AUTHORS ChangeLog README TODO || die
	use doc || rm -rf "${D}"/usr/share/gtk-doc
}
