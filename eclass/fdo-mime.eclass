# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/eclass/fdo-mime.eclass,v 1.7 2008/05/06 08:11:45 pva Exp $

# @ECLASS: fdo-mime.eclass
# @MAINTAINER:
# freedesktop-bugs@gentoo.org
# Original author: foser <foser@gentoo.org>
# @BLURB: Utility eclass to update the desktop mime info as laid out in the freedesktop specs & implementations
# @DESCRIPTION:
#
# To make use of this eclass simply call relevan need_update_desktopdb or
# need_update_mimedb functions after *DEPEND specification in ebuild. That all!
# If you need to redefine pkg_postinst or pkg_postrm functions then add relevant
# fdo-mime_{desktop,mime}_database_update functions inside.
#
# @EXAMPLE:
#
# Here is an example of an ebuild requiring update-desktop-database called after
# installation:
#
# @CODE
# DEPEND="x11-libs/gtk+"
# RDEPEND="${DEPEND}"
# need_update_desktopdb
# @CODE


UPDATE_DESKTOPDB_DEPEND="dev-util/desktop-file-utils"
UPDATE_MIMEDB_DEPEND="x11-misc/shared-mime-info"

# @FUNCTION: need_update_desktopdb
# @DESCRIPTION:
# An ebuild calls this to get correct dependencies for
# fdo-mime_desktop_database_update().
need_update_desktopdb() {
	debug-print-function $FUNCNAME $*
	DEPEND="${DEPEND} ${UPDATE_DESKTOPDB_DEPEND}"
	RDEPEND="${RDEPEND} ${UPDATE_DESKTOPDB_DEPEND}"
	NEED_UPDATE_DESKTOPDB="true"
}

# @FUNCTION: need_update_mimedb
# @DESCRIPTION:
# An ebuild calls this to get correct dependencies for
# fdo-mime_mime_database_update().
need_update_mimedb() {
	debug-print-function $FUNCNAME $*
	DEPEND="${DEPEND} ${UPDATE_MIMEDB_DEPEND}"
	RDEPEND="${RDEPEND} ${UPDATE_MIMEDB_DEPEND}"
	NEED_UPDATE_MIMEDB="true"
}

# @FUNCTION: fdo-mime_desktop_database_update
# @DESCRIPTION:
# Updates the desktop database.
# Generates a list of mimetypes linked to applications that can handle them
fdo-mime_desktop_database_update() {
	debug-print-function $FUNCNAME $*
	if [ -x "${ROOT}/usr/bin/update-desktop-database" ]
	then
		einfo "Updating desktop mime database ..."
		"${ROOT}/usr/bin/update-desktop-database" -q "${ROOT}/usr/share/applications"
	fi
}

# @FUNCTION: fdo-mime_mime_database_update
# @DESCRIPTION:
# Update the mime database.
# Creates a general list of mime types from several sources
fdo-mime_mime_database_update() {
	debug-print-function $FUNCNAME $*
	if [ -x "${ROOT}/usr/bin/update-mime-database" ]
	then
		einfo "Updating shared mime info database ..."
		"${ROOT}/usr/bin/update-mime-database" "${ROOT}/usr/share/mime"
	fi
}

# @FUNCTION: fdo-mime_pkg_postinst
# @DESCRIPTION:
# Calls relevant fdo-mime_* functions taking into account what was
# need_update_*'ed.
fdo-mime_pkg_postinst() {
	debug-print-function $FUNCNAME $*
	if [[ $NEED_UPDATE_DESKTOPDB == "true" ]]; then
		fdo-mime_desktop_database_update
	fi
	if [[ $NEED_UPDATE_MIMEDB == "true" ]]; then
		fdo-mime_mime_database_update
	fi
}

# @FUNCTION: fdo-mime_pkg_postrm
# @DESCRIPTION:
# Calls relevant fdo-mime_* functions taking into account what was
# need_update_*'ed.
fdo-mime_pkg_postrm() {
	debug-print-function $FUNCNAME $*
	if [[ $NEED_UPDATE_DESKTOPDB == "true" ]]; then
		fdo-mime_desktop_database_update
	fi
	if [[ $NEED_UPDATE_MIMEDB == "true" ]]; then
		fdo-mime_mime_database_update
	fi
}

EXPORT_FUNCTIONS pkg_postinst pkg_postrm
