# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DICT_PREFIX="dictd_www.mova.org_"
DICT_SUFFIX="1000pbio"

inherit stardict

DESCRIPTION="More than 1000+ short biographic articles (in Russian)"
HOMEPAGE="http://stardict.sourceforge.net/Dictionaries_dictd-www.mova.org.php"

KEYWORDS="~x86 ~amd64"
IUSE=""
