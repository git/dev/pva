# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

DICT_PREFIX=""
DICT_SUFFIX="dal-ru"

inherit stardict

DESCRIPTION="Thesaurus of Vladimir Ivanovich Dal (in Russian)"
HOMEPAGE="http://stardict.sourceforge.net/Dictionaries_dictd-www.mova.org.php"

KEYWORDS="~x86 ~amd64"
IUSE=""
