# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

FROM_LANG="Russian"
TO_LANG="English"
DICT_PREFIX="dictd_www.mova.org_"
DICT_SUFFIX="slovnyk_ru-en"

inherit stardict

DESCRIPTION="Russian to English word-list"
HOMEPAGE="http://stardict.sourceforge.net/Dictionaries_dictd-www.mova.org.php"

KEYWORDS="~x86 ~amd64"
IUSE=""
