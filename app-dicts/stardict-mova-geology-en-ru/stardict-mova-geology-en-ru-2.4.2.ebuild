# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

FROM_LANG="English"
TO_LANG="Russian"
DICT_PREFIX="dictd_www.mova.org_"
DICT_SUFFIX="geology_enru"

inherit stardict

DESCRIPTION="English to Russian dictionary of geology terms"
HOMEPAGE="http://stardict.sourceforge.net/Dictionaries_dictd-www.mova.org.php"

KEYWORDS="~x86 ~amd64"
IUSE=""
