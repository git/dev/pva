# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="2"
inherit subversion autotools multilib

REV=${PV#*_p}
DESCRIPTION="Tools for using C-motech hardware with Linux"
HOMEPAGE="http://cmotech-tools.sourceforge.net/"
ESVN_REPO_URI="http://cmotech-tools.svn.sourceforge.net/svnroot/cmotech-tools/trunk@${REV}"

LICENSE="GPLv3"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

DEPEND=""
RDEPEND=""

src_prepare() {
	eautoreconf
}

src_install() {
	make DESTDIR="${D}" install || die
	dodoc README || die
	insinto /etc/udev/rules.d
	doins cmotech.rules || die
	exeinto /$(get_libdir)/udev/
	doexe cmotech-cdswitch.sh || die
}
