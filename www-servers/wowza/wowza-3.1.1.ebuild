# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit versionator

MY_PN="WowzaMediaServer"
MY_P="${MY_PN}-${PV%_p*}"
MY_PP="${MY_PN}-$(replace_all_version_separators - ${PV%_p*})"
JOLOKIA=jolokia-1.0.2-bin
DESCRIPTION="High-performance server for unified media streaming to any screen (rtmp, hls, hds)"
HOMEPAGE="http://www.wowza.com/"
SRC_URI="http://www.wowza.com/downloads/${MY_PP}/${MY_P}.tar.bin
bwcheck? ( http://www.wowzamedia.com/downloads/forums/bwcheck/wms-plugin-bwcheck.zip )"
#jolokia? ( http://www.jolokia.org/dist/1.0.2/${JOLOKIA}.zip )"

if [[ ${PV/_p} != ${PV} ]]; then
	SRC_URI+=" http://www.wowza.com/downloads/${MY_PP}/${MY_PN}${PV/_p/-patch}.zip"
fi

LICENSE="wowza-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE="+bwcheck"
RESTRICT=""

DEPEND="app-arch/unzip"
RDEPEND="virtual/jdk"

WDESTDIR="/opt/wowza"
EXT_DIR="/media/store1/support/wowza-jars/"

SED_LOCAL_FILES="bin/WowzaMediaServerOSX bin/setenv.sh bin/WowzaMediaServer
README.html examples/VideoChat/install.sh
examples/SimpleVideoStreaming/install.sh examples/SHOUTcast/install.sh
examples/BWChecker/install.sh examples/VideoRecording/install.sh
examples/installall.sh examples/TextChat/install.sh
examples/ServerSideModules/install.sh
examples/ServerSideModules/server/build.xml
examples/LiveVideoStreaming/install.sh examples/LiveDvrStreaming/install.sh
examples/RemoteSharedObjects/install.sh"
if [[ ${PV/_p} != ${PV} ]]; then
	SED_LOCAL_FILES+=" README.txt"
fi

QA_PREBUILT="opt/wowza/${MY_P}/lib-native/linux64/*"
QA_EXECSTACK="opt/wowza/${MY_P}/lib-native/linux64/*"

src_unpack() {
	mkdir -p "${S}"
	cp "${DISTDIR}/${MY_P}.tar.bin" "${S}" || die
	if [[ ${PV/_p} != ${PV} ]]; then
		unpack "${MY_PN}${PV/_p/-patch}.zip"
	fi
	use bwcheck && unpack wms-plugin-bwcheck.zip
	#use jolokia && unpack ${JOLOKIA}.zip
	if [[ -d ${EXT_DIR} ]]; then
		mkdir libs
		for lib in "${EXT_DIR}"/*.jar; do
			elog "Preparing to install ${lib##*/} extension."
			cp "${lib}" "${WORKDIR}/libs" || die
		done
	fi
}

src_prepare() {
	sed -i -e '1,5{s|^more <<"EOF"|true <<"EOF"|}' \
		-e "1,855{s|^agreed=.*|agreed=yes|;s|^baseDir=.*|baseDir=${ED}${WDESTDIR}/|;s|^inetDest=.*|inetDest=${ED}/etc/init.d/|;}" \
		-e "1,855{s|/usr/local/WowzaMediaServer|${WDESTDIR}/WowzaMediaServer|g}" "${S}/${MY_P}.tar.bin"
	chmod +x "${S}/${MY_P}.tar.bin" || die
	if [[ ${PV/_p} != ${PV} ]]; then
		rm -rf "${WORKDIR}/${MY_PN}${PV/_p/-patch}/lib-native/win64" || die
	fi
	echo "CONFIG_PROTECT=\"${EPREFIX}${WDESTDIR}/${MY_P}/conf ${EPREFIX}${WDESTDIR}/${MY_P}/bin/setenv.sh\"" >> "${T}/50${PN}"
}

src_compile() { :; }

src_install() {
	dodir ${WDESTDIR}
	./${MY_P}.tar.bin || die

	if [[ ${PV/_p} != ${PV} ]]; then
		einfo "Applying ${MY_PN}${PV/_p/-patch}.zip"
		pushd "${WORKDIR}/${MY_PN}${PV/_p/-patch}" >/dev/null
		find . -type d -exec mkdir -v -p "${ED}${WDESTDIR}/${MY_P}"/'{}' \; || die
		find . -type f -exec cp -v '{}' "${ED}${WDESTDIR}/${MY_P}"/'{}' \; || die
		popd >/dev/null
	fi
	# Fix execution permissions on all files
	find "${ED}${WDESTDIR}/${MY_P}/" -type f -exec chmod -x '{}' \;
	chmod +x "${ED}${WDESTDIR}/${MY_P}/bin"/*.sh || die
	chmod +x "${ED}${WDESTDIR}/${MY_P}/bin"/WowzaMediaServer* || die

	if use bwcheck; then
		cp -v "${WORKDIR}/wms-plugin-bwcheck/wms-plugin-bwcheck.jar" "${ED}${WDESTDIR}/${MY_P}/lib/" || die
	fi
	for lib in "${WORKDIR}/libs"/*; do
		elog "Installing ${lib##*/}..."
		cp -v "${lib}" "${ED}${WDESTDIR}/${MY_P}/lib/" || die
	done

	pushd "${ED}${WDESTDIR}/WowzaMediaServer/" >/dev/null
	sed -i -e "s|/usr/local|${WDESTDIR}|" ${SED_LOCAL_FILES}
	while read file; do
		ewarn "QQA: /usr/local path in file. Update SED_LOCAL_FILES ($file)"
	done < <(find "${ED}" -type f -exec grep -H -e "/usr/local" '{}' \;)
	popd >/dev/null

	doenvd "${T}/50${PN}"
}
